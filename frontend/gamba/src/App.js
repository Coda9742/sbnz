import "./App.css";
import Content from "./components/Content";
import Login from "./components/Login";
import History from "./components/History";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
        <Routes>
          <Route path="/" element={<Login />} exact />
          <Route path="/login" element={<Login />} exact />
          <Route path="/home" element={<Content />} exact />
          <Route path="/history" element={<History />} exact />
      </Routes>
    </Router>
  );
}

export default App;
