import React, { useState } from 'react';
import Swal from 'sweetalert2';

function Navbar() {

    const [refreshLOL, setRefreshLOL] = useState(0);

    const deposit = () => {
        Swal.fire({
            title: 'Unesite pare',
            input: 'number',
            showCancelButton: true,
            confirmButtonText: 'Uplati',
            preConfirm: (amount) => {
                return fetch(`http://localhost:8080/api/client/deposit`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(
                        {
                            amount: amount,
                            clientId: localStorage.getItem('id')
                        }
                    )
                })
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.isConfirmed) {
                fetch('http://localhost:8080/api/client/' + localStorage.getItem('id'), {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                    .then(response => response.json())
                    .then(response => {
                        console.log(JSON.stringify(response))
                        localStorage.setItem('id', response['id'])
                        localStorage.setItem('email', response['email'])
                        localStorage.setItem('firstName', response['firstName'])
                        localStorage.setItem('lastName', response['lastName'])
                        localStorage.setItem('balance', response['balance'])
                        localStorage.setItem('freebet', response['freebet'])
                        localStorage.setItem('lockedFreebet', response['lockedFreebet'])
                        setRefreshLOL(refreshLOL + 1);

                    })
                Swal.fire(
                    'Good job!',
                    'Your ticket has been placed',
                    'success'
                )
            }
        })
    }
    return (
        <div >
            <header style={headerStyle}>
                <h1 style={titleStyle}>GambaBet</h1>
                {
                    localStorage.getItem('id') &&
                    <div>
                        <p>{localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName')}</p>
                        <p>Balance: {localStorage.getItem('balance')}</p>
                        <p>Freebet: {localStorage.getItem('freebet')}</p>
                        <p>Locked Freebet: {localStorage.getItem('lockedFreebet')}</p>
                        <button type="button" class="btn btn-primary" onClick={deposit}>Depozit</button>
                    </div>

                }
            </header>

        </div>
    );
}

const headerStyle = {
    backgroundColor: 'black',
    color: 'lightblue',
    textAlign: 'center',
    padding: '1rem',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
};

const titleStyle = {
    margin: '0',
    fontSize: '3rem',
    fontFamily: 'Times New Roman, serif',
};


const buttonStyle = {
    fontSize: '1rem',
    padding: '0.5rem 1rem',
    backgroundColor: 'lightblue',
    color: 'black',
    border: 'none',
    borderRadius: '4px',
};

export default Navbar