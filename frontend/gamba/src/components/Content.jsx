import React, { useState } from "react";
import Navbar from "./Navbar";
import style from "../styles/Content.module.css";
import Swal from 'sweetalert2'

const KVOTE = [
  {
    id: 1,
    home: "Arsenal",
    away: "Chelsea",
    hw: 1.83,
    d: 3.74,
    aw: 4.41,
  },
  {
    id: 2,
    home: "Ferencvaros",
    away: "CFR Cluj",
    hw: 2.83,
    d: 3.51,
    aw: 2.41,
  },
  {
    id: 3,
    home: "Javor",
    away: "Vozdovac",
    hw: 2.65,
    d: 3.05,
    aw: 2.95,
  },
  {
    id: 4,
    home: "Olympiacos",
    away: "Ludogorets",
    hw: 2.25,
    d: 3.35,
    aw: 3.85,
  },
  {
    id: 5,
    home: "Ipswich",
    away: "Oldham",
    hw: 1.95,
    d: 3.65,
    aw: 4.35,
  },
  {
    id: 6,
    home: "BATE Borisov",
    away: "Dynamo Kiev",
    hw: 2.55,
    d: 3.35,
    aw: 2.81,
  },
  {
    id: 7,
    home: "Twente",
    away: "Anderlecht",
    hw: 2.75,
    d: 3.11,
    aw: 2.91,
  },
  {
    id: 8,
    home: "Yokohama",
    away: "Kyoto",
    hw: 1.45,
    d: 3.85,
    aw: 5.25,
  },
  {
    id: 9,
    home: "River Plate",
    away: "BOCA Juniors",
    hw: 1.95,
    d: 3.65,
    aw: 2.95,
  },
  {
    id: 10,
    home: "Rudar Prijedor",
    away: "Borac Banja Luka",
    hw: 1.25,
    d: 3.65,
    aw: 5.95,
  },
];



const Content = () => {
  const [tiket, setTiket] = useState([]);
  const [sumOdd, setSumOdd] = useState(1);
  const [amount, setAmount] = useState(0);
  const [refreshLOL, setRefreshLOL] = useState(0);

  const handleOddClick = (match, tip, odd) => {
    const { home, away } = match;
    setTiket([
      ...tiket,
      {
        home,
        away,
        tip,
        odd,
      },
    ]);

    setSumOdd((prev) => prev * odd);
  };

  //   console.log(tiket);

  //   
  const placeBetHandler = () => {
    console.log(amount);
    console.log(tiket)
    console.log(localStorage.getItem('id'))
    fetch('http://localhost:8080/api/client/bet', {
                method: 'POST',
                headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      clientId: localStorage.getItem('id'),
      pairs : tiket,
      betAmount : amount
    })
})
   .then(response => response.json())
   .then(response => {
    fetch('http://localhost:8080/api/client/' + localStorage.getItem('id'), {
                method: 'GET',
                headers: {
        'Content-Type': 'application/json'
    }
})
   .then(response => response.json())
   .then(response => {
    console.log(JSON.stringify(response))
    localStorage.setItem('id', response['id'])
    localStorage.setItem('email', response['email'])
    localStorage.setItem('firstName', response['firstName'])
    localStorage.setItem('lastName', response['lastName'])
    localStorage.setItem('balance', response['balance'])
    localStorage.setItem('freebet', response['freebet'])
    localStorage.setItem('lockedFreebet', response['lockedFreebet'])
    setRefreshLOL(refreshLOL + 1);

  })
    Swal.fire(
      'Good job!',
      'Your ticket has been placed',
      'success'
    )

  })
  };

  return (
    <div>
      <Navbar />
    
    <div className={style["main"]}>
      <div id="kvote">
        <table className={style["table"]}>
          <tr>
            <th>Home</th>
            <th>Away</th>
            <th>1</th>
            <th>X</th>
            <th>2</th>
          </tr>
          {KVOTE.map((match) => {
            return (
              <tr className={style["match-row"]} key={match.id}>
                <td>{match.home}</td>
                <td>{match.away}</td>
                <td
                  className={style["odd"]}
                  onClick={() => handleOddClick(match, "1", match.hw)}
                >
                  {match.hw}
                </td>
                <td
                  className={style["odd"]}
                  onClick={() => handleOddClick(match, "X", match.d)}
                >
                  {match.d}
                </td>
                <td
                  className={style["odd"]}
                  onClick={() => handleOddClick(match, "2", match.aw)}
                >
                  {match.aw}
                </td>
              </tr>
            );
          })}
        </table>
      </div>
      <div id="tiket" className={style["tiket"]}>
        <table className={style["tiket-table"]}>
          <tr>
            <th>Home</th>
            <th>Away</th>
            <th>Tip</th>
            <th>Odd</th>
          </tr>
          {tiket.map((match) => {
            return (
              <tr className={style["match-row"]}>
                <td>{match.home}</td>
                <td>{match.away}</td>
                <td
                //   className={style["odd"]}
                >
                  {match.tip}
                </td>
                <td
                //   className={style["odd"]}
                >
                  {match.odd}
                </td>
              </tr>
            );
          })}
        </table>
        <div>
          <div id="info-section" className={style["info-box"]}>
            <div className={style["info"]}>
              <div>Number of matches:</div>
              <div>{tiket.length}</div>
            </div>
            <div className={style["info"]}>
              <div>Odd:</div>
              <div>{sumOdd.toFixed(2)}</div>
            </div>
            <div className={style["info"]}>
              <div>Payout: </div>
              <div>{(amount * sumOdd).toFixed(2)}</div>
            </div>
          </div>
        </div>
        <div id="payment-section" className={style["payment"]}>
          <input
            className={style["money-input"]}
            type="number"
            onChange={(e) => setAmount(e.target.value)}
          />
          <button className={style["place-bet"]} onClick={placeBetHandler}>
            Place bet
          </button>
        </div>
      </div>
    </div>
    </div>
  );
};

export default Content;
