import React, {useEffect, useState} from 'react'
import style from "../styles/Content.module.css";
import Navbar from './Navbar';



function History() {

    const [tickets, setTikets] = useState([]);
    const [pairs, setPairs] = useState([]);

  const getData = () => 
  {
    fetch('http://localhost:8080/api/client/' + localStorage.getItem('id') + '/ticketHistory', {
                method: 'GET',
                headers: {
        'Content-Type': 'application/json'
    }
})
   .then(response => response.json())
   .then(response => {
    setTikets(response)
    console.log(response)

  })
  }


    useEffect(() => {
        getData()
      }, [])

      
      const handleShowPairs = ticketId =>
      {
        for (let i = 0; i < tickets.length; i++)
        {
            if (ticketId === tickets[i].id) setPairs(tickets[i].pairs)
        }
      }

  return (
    <div>
        <Navbar />
        <table className={style["table"]}>
          <tr>
            <th>TicketID &nbsp; &nbsp; &nbsp;</th>
            <th>Num. of pairs &nbsp; &nbsp;</th>
            <th>Bet Amount &nbsp; &nbsp;</th>
            <th>Full odds &nbsp; &nbsp;</th>
            <th>Regular payout &nbsp; &nbsp;</th>
            <th>Bonus &nbsp; &nbsp;</th>
            <th>Full payout &nbsp; &nbsp;</th>
          </tr>
          {tickets != null && tickets.map((ticket) => {
            return (
              <tr className={style["match-row"]} key={ticket.id}>
                <td>{ticket.id}</td>
                <td>{ticket.numOfPairs}</td>
                <td>{ticket.betAmount}</td>
                <td>{ticket.fullOdd.toFixed(2)} </td>
                <td>{ticket.regularPayout.toFixed(2)}</td>
                <td>{ticket.bonus.toFixed(2)}</td>
                <td>{(ticket.regularPayout + ticket.bonus).toFixed(2)}</td>
                <td><button onClick={() => handleShowPairs(ticket.id)}>Show Pairs</button></td>
              </tr>
            );
          })}
        </table>
        <div>
            {
                pairs.map(pair =>
                    {
                        return (
                        <table>
                            <tr>
                                <th>Home &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                                <th>Away &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                                <th>Tip &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                                <th>Odd &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                            </tr>
                            <tr className={style["match-row"]} key={pair.id}>
                                <td>{pair.home}</td>
                                <td>{pair.away}</td>
                                <td>{pair.tip}</td>
                                <td>{pair.odd}</td>
                            </tr>
                        </table>
                        )
                    })
            }
        </div>
    </div>
  )
}

export default History