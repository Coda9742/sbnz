import React from 'react'
import { useNavigate } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import Swal from 'sweetalert2'
import Navbar from './Navbar';

function Login() {

  const navigate = useNavigate();

    let loginForm = 
    {
        email : null,
        password : null
    }

    const handleEmailChange = e =>
    {
        loginForm.email = e.target.value;
    }

    const handlePasswordChange = e =>
    {
        loginForm.password = e.target.value;
    }
    const logIn = e =>
    {
        e.preventDefault();
        console.log(loginForm)
        if (loginForm.email === null || loginForm.password === null)
        {
            Swal.fire({
                icon: 'error',
                title: 'Bad Credentianls',
                text: 'Input both email and password',
              })
        }

        fetch('http://localhost:8080/api/client/login', {
                method: 'POST',
                headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(loginForm)
})
   .then(response => response.json())
   .then(response => {
    console.log(JSON.stringify(response))
    localStorage.setItem('id', response['id'])
    localStorage.setItem('email', response['email'])
    localStorage.setItem('firstName', response['firstName'])
    localStorage.setItem('lastName', response['lastName'])
    localStorage.setItem('balance', response['balance'])
    localStorage.setItem('freebet', response['freebet'])
    localStorage.setItem('lockedFreebet', response['lockedFreebet'])
    navigate("/home")

  })
   .catch(err => 
    {
        Swal.fire({
            icon: 'error',
            title: 'Bad Credentianls',
            text: 'Wrong username or password',
          })
    })
    }
    return (
        <div>
          <Navbar />
        <div className="AuthFormContainer">
          <form className="AuthForm">
            <div className="AuthFormContent">
              <h3 className="AuthFormTitle">Sign In</h3>
              <div className="form-group mt-3">
                <label>Email address</label>
                <input
                  type="email"
                  className="form-control mt-1"
                  placeholder="Enter email"
                  onChange={handleEmailChange}
                />
              </div>
              <div className="form-group mt-3">
                <label>Password</label>
                <input
                  type="password"
                  className="form-control mt-1"
                  placeholder="Enter password"
                  onChange={handlePasswordChange}
                />
              </div>
              <div className="d-grid gap-2 mt-3">
                <button type="submit" className="btn btn-primary" onClick={logIn}>
                  Submit
                </button>
              </div>
            </div>
          </form>
        </div>
        </div>
      )
    }



const App = {
        backgroundColor: 'white'
      }
      
const AuthFormContainer = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100vw',
        height: '100vh',
      }
      
const AuthForm = {
        width: '420px',
        boxShadow: 'rgb(0 0 0 / 16%) 1px 1px 10px',
        paddingTop: '30px',
        paddingBottom: '20px',
        borderRadius: '8px',
        backgroundColor: 'white'
      }
      
const AuthFormContent = {
        paddingLeft: '12%',
        paddingRight: '12%',
      }
      
const AuthFormTitle = {
        textAlign: 'center',
        marginBottom: '1em',
        fontSize: '24px',
        color: 'rgb(34, 34, 34)',
        fontWeight: '800'
      }
      
const label =  {
        fontSize: '14px',
        fontWeight: '600',
        color: 'rgb(34, 34, 34)'
      }

    
export default Login