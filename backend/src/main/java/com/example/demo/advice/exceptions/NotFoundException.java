package com.example.demo.advice.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotFoundException extends RuntimeException{

    private HttpStatus status;
    private String message;
}
