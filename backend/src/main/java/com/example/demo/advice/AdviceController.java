package com.example.demo.advice;

import com.example.demo.advice.exceptions.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AdviceController {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleRedirect302Exception(NotFoundException e)
    {
        return new ResponseEntity<String>(e.getMessage(), e.getStatus());
    }
}