package com.example.demo.models.classificators;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassificationFreebetUnlockerModel {

    private Double percentage;
    private Double minOdds;
    private Double maxOdds;
    private Integer minNumOfPairs;
    private Integer maxNumOfPairs;
}
