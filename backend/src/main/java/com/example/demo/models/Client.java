package com.example.demo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Client {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private Double balance;

    private Double lockedFreebet;
    private Double freebet;

    private Integer numOfDeposits;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Ticket> tickets;

    private String password;


    public void depositMoney(Deposit deposit)
    {
        this.balance += deposit.getAmount();
        this.lockedFreebet += deposit.getLockedFreebet();
        this.numOfDeposits += 1;
    }

    public void transferLockedToFreebet(Double bet, Double percentage)
    {
        if (this.lockedFreebet < bet * percentage) return;
        this.lockedFreebet -= bet * percentage;
        this.freebet += bet * percentage;
    }

}