package com.example.demo.models;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Deposit {

    private Double amount;
    private Long clientId;
    private Integer depositNum;
    private Double lockedFreebet = 0.0;

    public Deposit(Double amount, Long clientId, Integer depositNum)
    {
        this.amount = amount;
        this.clientId = clientId;
        this.depositNum = depositNum;
    }

    public Deposit(Double amount, Long clientId)
    {
        this.amount = amount;
        this.clientId = clientId;
    }

    public void addFreebet(Double bonus)
    {
        switch (this.depositNum)
        {
            case 1:
                if (amount * bonus < 6000.0)
                {
                    this.lockedFreebet = amount * bonus;
                    break;
                }
                this.lockedFreebet = 6000.0;
                break;
            case 2:
                if (amount * bonus < 12000.0)
                {
                    this.lockedFreebet = amount * bonus;
                    break;
                }
                this.lockedFreebet = 12000.0;
                break;
            case 3:
                if (amount * bonus < 24000.0)
                {
                    this.lockedFreebet = amount * bonus;
                    break;
                }
                this.lockedFreebet = 24000.0;
                break;
            default:
                this.lockedFreebet = 0.0;
                break;
        }
    }
}
