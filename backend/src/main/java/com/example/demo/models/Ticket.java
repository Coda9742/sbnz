package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long clientId;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Pair> pairs;

    private Integer numOfPairs;

    private Double fullOdd = 0.0;

    private Double betAmount;

    private Double regularPayout;

    private Double bonus;

    private Double possiblePayout;


    public Ticket(Long clientId, List<Pair> pairs, Double betAmount)
    {
        this.clientId = clientId;
        this.pairs = pairs;
        this.betAmount = betAmount;
        this.numOfPairs = pairs.size();
        this.fullOdd = pairs.stream()
                .mapToDouble(Pair::getOdd)
                .reduce(1, (a, b) -> a * b);
        this.regularPayout = betAmount * this.fullOdd;
    }

    public void addPair(Pair pair)
    {
        if (this.fullOdd == 0.0)
        {
            this.fullOdd = pair.getOdd();
            return;
        }
        this.pairs.add(pair);
        this.numOfPairs++;
        this.fullOdd = this.fullOdd * pair.getOdd();
    }

    public void addBonus(Double bonus)
    {
        switch (numOfPairs)
        {
            case 5 -> this.bonus = this.regularPayout * 0.03;
            case 6 -> this.bonus = this.regularPayout * 0.05;
            case 7 -> this.bonus = this.regularPayout * 0.1;
            case 8 -> this.bonus = this.regularPayout * 0.15;
            case 9 -> this.bonus = this.regularPayout * 0.2;
            case 10 -> this.bonus = this.regularPayout * 0.3;
            case 11 -> this.bonus = this.regularPayout * 0.4;
            case 12 -> this.bonus = this.regularPayout * 0.5;
            default -> this.bonus = 0.0;
        }

        if (this.fullOdd >= numOfPairs * 2.5) this.bonus += this.bonus;
        this.possiblePayout = this.regularPayout + this.bonus;
    }

    public void calculateFields()
    {
        this.numOfPairs = pairs.size();
        this.fullOdd = pairs.stream()
                .mapToDouble(Pair::getOdd)
                .reduce(1, (a, b) -> a * b);
        this.regularPayout = betAmount * this.fullOdd;
        this.bonus = 0.0;
        this.possiblePayout = 0.0;
    }


}
