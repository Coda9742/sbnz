package com.example.demo.models.classificators;

public class ClassificationDepositModel
{
    private Integer depositNum;
    private Double bonus;

    public ClassificationDepositModel()
    {

    }

    public ClassificationDepositModel(Integer depositNum, Double bonus) {
        this.depositNum = depositNum;
        this.bonus = bonus;
    }

    public Integer getDepositNum() {
        return depositNum;
    }

    public void setDepositNum(Integer depositNum) {
        this.depositNum = depositNum;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }
}
