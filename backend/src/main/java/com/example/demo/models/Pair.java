package com.example.demo.models;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Pair {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String home;
    private String away;
    private String tip;
    private Double odd;
}
