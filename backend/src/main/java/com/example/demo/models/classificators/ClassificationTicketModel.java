package com.example.demo.models.classificators;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassificationTicketModel {

    private Integer numOfPairs;
    private Double bonus;

}
