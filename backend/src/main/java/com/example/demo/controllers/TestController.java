package com.example.demo.controllers;

import com.example.demo.models.Client;
import com.example.demo.repos.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private ClientRepo clientRepo;

    @GetMapping("/")
    public String fireAllRules()
    {
        return "<h1>test</h1>";

    }
}
