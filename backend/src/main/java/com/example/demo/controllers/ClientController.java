package com.example.demo.controllers;

import com.example.demo.models.Client;
import com.example.demo.models.Deposit;
import com.example.demo.models.LoginRequest;
import com.example.demo.models.Ticket;
import com.example.demo.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/client")
@CrossOrigin
public class ClientController {

    private ClientService clientService;
    @Autowired
    public ClientController(ClientService clientService)
    {
        this.clientService = clientService;
    }

    @PostMapping("/deposit")
    public ResponseEntity<Client> deposit(@RequestBody Deposit deposit)
    {
        return ResponseEntity.ok().body(clientService.makeDeposit(deposit));
    }

    @PostMapping("/bet")
    public ResponseEntity<Ticket> bet(@RequestBody Ticket ticket)
    {
        ticket.calculateFields();
        return  ResponseEntity.ok().body(clientService.bet(ticket));
    }

    @PostMapping("/login")
    public ResponseEntity<Client> login(@RequestBody LoginRequest loginRequest)
    {
        return ResponseEntity.ok().body(clientService.login(loginRequest));
    }

    @GetMapping("/{id}/ticketHistory")
    public ResponseEntity<List<Ticket>> getTickets(@PathVariable("id") Long id)
    {
        return ResponseEntity.ok().body(clientService.getTicketsForClient(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Client> getClient(@PathVariable("id") Long id)
    {
        return ResponseEntity.ok().body(clientService.getClient(id));
    }




}
