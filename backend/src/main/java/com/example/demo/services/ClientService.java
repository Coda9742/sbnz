package com.example.demo.services;


import com.example.demo.advice.exceptions.NotFoundException;
import com.example.demo.advice.exceptions.PoorBastardException;
import com.example.demo.models.LoginRequest;
import com.example.demo.models.classificators.ClassificationDepositModel;
import com.example.demo.models.Client;
import com.example.demo.models.Deposit;
import com.example.demo.models.Ticket;
import com.example.demo.models.classificators.ClassificationFreebetUnlockerModel;
import com.example.demo.models.classificators.ClassificationTicketModel;
import com.example.demo.repos.ClientRepo;
import com.example.demo.repos.TicketRepo;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {

    private final ClientRepo clientRepo;

    private final TicketRepo ticketRepo;
    private KieSession depositSession;

    private KieSession ticketSession;

    private KieSession freeBetUnlockerSession;

    @Autowired
    public ClientService(ClientRepo clientRepo, TicketRepo ticketRepo)
    {
        this.clientRepo = clientRepo;
        this.ticketRepo = ticketRepo;
        this.depositSession = createDepositSessionFromDrl();
        this.ticketSession = createTicketSessionFromDrl();
        this.freeBetUnlockerSession = createFreebetUnlockerSessionFromDrl();
    }


    public Client makeDeposit(Deposit deposit)
    {
        if (!clientRepo.existsById(deposit.getClientId())) throw new NotFoundException(HttpStatus.NOT_FOUND, "No client with such ID");
        Client client = clientRepo.findById(deposit.getClientId()).get();
        deposit.setDepositNum(client.getNumOfDeposits() + 1);
        this.depositSession.insert(deposit);
        this.depositSession.fireAllRules();
        client.depositMoney(deposit);
        this.depositSession.dispose();;
        this.depositSession = createDepositSessionFromDrl();
        return clientRepo.save(client);
    }

    public Ticket bet(Ticket ticket)
    {
        if (!clientRepo.existsById(ticket.getClientId())) throw new NotFoundException(HttpStatus.NOT_FOUND, "No client with such ID");
        Client client = clientRepo.findById(ticket.getClientId()).get();
        if (client.getBalance() < ticket.getBetAmount()) throw new PoorBastardException(HttpStatus.BAD_REQUEST, "You ain't got the moneyz brev");
        this.ticketSession.insert(ticket);
        this.ticketSession.fireAllRules();
        this.freeBetUnlockerSession.insert(ticket);
        this.freeBetUnlockerSession.insert(client);
        this.freeBetUnlockerSession.fireAllRules();
        client.getTickets().add(ticket);
        client.setBalance(client.getBalance() - ticket.getBetAmount());
        this.ticketSession.dispose();
        this.ticketSession = createTicketSessionFromDrl();
        this.freeBetUnlockerSession.dispose();
        this.freeBetUnlockerSession = createFreebetUnlockerSessionFromDrl();
        clientRepo.save(client);
        return ticket;
    }

    public Client login(LoginRequest loginRequest)
    {
        if (! clientRepo.existsByEmail(loginRequest.getEmail())) throw new NotFoundException(HttpStatus.NOT_FOUND, "No client with such email");
        Client client = clientRepo.findByEmail(loginRequest.getEmail()).get();
        if (!client.getPassword().equals(loginRequest.getPassword())) throw new NotFoundException(HttpStatus.NOT_FOUND, "Wrong password");
        return client;
    }

    public List<Ticket> getTicketsForClient(Long id)
    {
        if (!clientRepo.existsById(id)) throw new NotFoundException(HttpStatus.NOT_FOUND, "No client with such ID");
        return clientRepo.findById(id).get().getTickets();
    }

    public Client getClient(Long id)
    {
        if (!clientRepo.existsById(id)) throw new NotFoundException(HttpStatus.NOT_FOUND, "No client with such ID");
        return clientRepo.findById(id).get();
    }

    private KieSession createDepositSessionFromDrl(){
        InputStream template = ClientService.class.getResourceAsStream("/templates/deposit-classification.drt");

        List<ClassificationDepositModel> data = new ArrayList<ClassificationDepositModel>();

        data.add(new ClassificationDepositModel(1, 0.6));
        data.add(new ClassificationDepositModel(2, 1.2));
        data.add(new ClassificationDepositModel(3, 2.5));
        ObjectDataCompiler converter = new ObjectDataCompiler();
        String drl = converter.compile(data, template);
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);

        Results results = kieHelper.verify();

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }

        return kieHelper.build().newKieSession();
    }

    private KieSession createTicketSessionFromDrl(){
        InputStream template = ClientService.class.getResourceAsStream("/templates/ticket-classification.drt");

        List<ClassificationTicketModel> data = new ArrayList<ClassificationTicketModel>();

        data.add(new ClassificationTicketModel(4, 0.0));
        data.add(new ClassificationTicketModel(5, 0.03));
        data.add(new ClassificationTicketModel(6, 0.05));
        data.add(new ClassificationTicketModel(7, 0.1));
        data.add(new ClassificationTicketModel(8, 0.15));
        data.add(new ClassificationTicketModel(9, 0.20));
        data.add(new ClassificationTicketModel(10, 0.30));
        data.add(new ClassificationTicketModel(11, 0.40));
        data.add(new ClassificationTicketModel(12, 0.50));

        ObjectDataCompiler converter = new ObjectDataCompiler();
        String drl = converter.compile(data, template);
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);

        Results results = kieHelper.verify();

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }

        return kieHelper.build().newKieSession();
    }

    private KieSession createFreebetUnlockerSessionFromDrl(){
        InputStream template = ClientService.class.getResourceAsStream("/templates/freebet-unlocking-classification.drt");

        List<ClassificationFreebetUnlockerModel> data = new ArrayList<ClassificationFreebetUnlockerModel>();

        data.add(new ClassificationFreebetUnlockerModel(0.2, 2.1, 3.5, 1, 2));
        data.add(new ClassificationFreebetUnlockerModel(0.4, 3.5, 4.0, 2, 3));
        data.add(new ClassificationFreebetUnlockerModel(0.6, 4.0, 5.0,3, 4));
        data.add(new ClassificationFreebetUnlockerModel(1.0, 5.0, 1000000.0, 4, 10000));

        ObjectDataCompiler converter = new ObjectDataCompiler();
        String drl = converter.compile(data, template);
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);

        Results results = kieHelper.verify();

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }

        return kieHelper.build().newKieSession();
    }



}
