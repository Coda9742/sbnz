package com.example.demo;

import com.example.demo.models.Pair;
import com.example.demo.models.Ticket;
import com.example.demo.models.classificators.ClassificationDepositModel;
import com.example.demo.models.Client;
import com.example.demo.models.Deposit;
import com.example.demo.models.classificators.ClassificationFreebetUnlockerModel;
import com.example.demo.models.classificators.ClassificationTicketModel;
import org.drools.template.ObjectDataCompiler;
import org.junit.jupiter.api.Test;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class TestGamblingBusiness {

	@Test
	public void testDepositToClient()
	{

		InputStream template = TestGamblingBusiness.class.getResourceAsStream("/templates/deposit-classification.drt");


		List<ClassificationDepositModel> data = new ArrayList<ClassificationDepositModel>();

		data.add(new ClassificationDepositModel(1, 0.6));
		data.add(new ClassificationDepositModel(2, 1.2));
		data.add(new ClassificationDepositModel(3, 2.5));

		ObjectDataCompiler converter = new ObjectDataCompiler();
		String drl = converter.compile(data, template);

		System.out.println(drl);

		KieSession ksession = createKieSessionFromDRL(drl);

		Deposit deposit1 = new Deposit(5000.0,1L, 1);
		Deposit deposit2 = new Deposit( 5000.0, 2L, 2);
		Deposit deposit3 = new Deposit( 5000.0, 3L, 3);

		ksession.insert(deposit1);
		ksession.insert(deposit2);
		ksession.insert(deposit3);

		ksession.fireAllRules();

		System.out.println(deposit1.getLockedFreebet());
		System.out.println(deposit2.getLockedFreebet());
		System.out.println(deposit3.getLockedFreebet());

		assertEquals(deposit1.getLockedFreebet(), 3000.0);
		assertEquals(deposit2.getLockedFreebet(), 6000.0);
		assertEquals(deposit3.getLockedFreebet(), 12500.0);

		Client client1 = new Client(1L, "mejl", "Imenko", "Prezimenkovic", 1000.0, 250.0, 0.0, 1, new ArrayList<>(), "");
		Client client2 = new Client(2L, "mejl","Imenko", "Prezimenkovic", 1200.0, 0.0, 0.0, 1, new ArrayList<>(), "");
		Client client3 = new Client(3L, "mejl","Imenko", "Prezimenkovic", 2200.0, 500.0, 0.0, 1, new ArrayList<>(), "");

		client1.depositMoney(deposit1);
		client2.depositMoney(deposit2);
		client3.depositMoney(deposit3);

		assertEquals(client1.getBalance(), 6000);
		assertEquals(client1.getLockedFreebet(), 3250);
		assertEquals(client2.getBalance(), 6200);
		assertEquals(client2.getLockedFreebet(), 6000);
		assertEquals(client3.getBalance(), 7200);
		assertEquals(client3.getLockedFreebet(), 13000);
	}

	@Test
	public void testBonusOnTicket()
	{
		InputStream template = TestGamblingBusiness.class.getResourceAsStream("/templates/ticket-classification.drt");

		List<ClassificationTicketModel> data = new ArrayList<ClassificationTicketModel>();

		data.add(new ClassificationTicketModel(4, 0.0));
		data.add(new ClassificationTicketModel(5, 0.03));
		data.add(new ClassificationTicketModel(6, 0.05));
		data.add(new ClassificationTicketModel(7, 0.1));
		data.add(new ClassificationTicketModel(8, 0.15));
		data.add(new ClassificationTicketModel(9, 0.20));
		data.add(new ClassificationTicketModel(10, 0.30));
		data.add(new ClassificationTicketModel(11, 0.40));
		data.add(new ClassificationTicketModel(12, 0.50));

		ObjectDataCompiler converter = new ObjectDataCompiler();
		String drl = converter.compile(data, template);
		System.out.println(drl);

		KieSession ksession = createKieSessionFromDRL(drl);

		List<Pair> pairs = new ArrayList<>();
		pairs.add(new Pair(1L, "HOME", "AWAY", "X", 4.0));
		pairs.add(new Pair(2L, "HOME", "AWAY", "X", 4.00));
		pairs.add(new Pair(3L, "HOME", "AWAY", "X", 4.00));
		pairs.add(new Pair(4L, "HOME", "AWAY", "X", 4.00));

		Ticket ticket = new Ticket(1L, pairs, 300.0);
		ksession.insert(ticket);
		ksession.fireAllRules();

		System.out.println(ticket.getFullOdd());

		assertEquals(ticket.getBonus(), 0.0);


		pairs.add(new Pair(4L, "HOME", "AWAY", "X", 4.0));
		Ticket ticket1 = new Ticket(1L, pairs, 300.0);
		ksession.insert(ticket1);
		ksession.fireAllRules();
		assertEquals(18432.0, ticket1.getBonus());


//		Ticket ticket2 = new Ticket(1L, pairs, 300.0);
//		ksession.insert(ticket2);
//		ksession.fireAllRules();
//		assertEquals(61440.0, ticket2.getBonus());

//		pairs.add(new Pair(4L, "HOME", "AWAY", "X", 2.0));
//		Ticket ticket3 = new Ticket(1L, pairs, 300.0);
//		ksession.insert(ticket3);
//		ksession.fireAllRules();
//		assertEquals(ticket3.getBonus(), 1000.0 * 2);
//
//		Ticket ticket4 = new Ticket(1L, pairs, 300.0);
//		ticket4.addPair(new Pair(4L, "HOME", "AWAY", "X", 3.05));
//		ksession.insert(ticket4);
//		ksession.fireAllRules();
//		assertEquals(ticket4.getBonus(), 1500.0 * 2);
//
//		Ticket ticket5 = new Ticket(1L, pairs, 300.0);
//		ticket5.addPair(new Pair(4L, "HOME", "AWAY", "X", 3.05));
//		ksession.insert(ticket5);
//		ksession.fireAllRules();
//		assertEquals(ticket5.getBonus(), 2000.0 * 2);
//
//		Ticket ticket6 = new Ticket(1L, pairs, 300.0);
//		ticket6.addPair(new Pair(4L, "HOME", "AWAY", "X", 3.05));
//		ksession.insert(ticket6);
//		ksession.fireAllRules();
//		assertEquals(ticket6.getBonus(), 3000.0 * 2);
//
//		Ticket ticket7 = new Ticket(1L, pairs, 300.0);
//		ticket7.addPair(new Pair(4L, "HOME", "AWAY", "X", 3.05));
//		ksession.insert(ticket7);
//		ksession.fireAllRules();
//		assertEquals(ticket7.getBonus(), 4000.0 * 2);
//
//		Ticket ticket8 = new Ticket(1L, pairs, 300.0);
//		ticket8.addPair(new Pair(4L, "HOME", "AWAY", "X", 3.05));
//		ksession.insert(ticket8);
//		ksession.fireAllRules();
//		assertEquals(ticket8.getBonus(), 5000.0 * 2);



	}

	@Test
	public void testLockedToFreebetTransfer()
	{
		InputStream template = TestGamblingBusiness.class.getResourceAsStream("/templates/freebet-unlocking-classification.drt");

		List<ClassificationFreebetUnlockerModel> data = new ArrayList<ClassificationFreebetUnlockerModel>();

		data.add(new ClassificationFreebetUnlockerModel(0.2, 2.1, 3.5, 1, 2));
		data.add(new ClassificationFreebetUnlockerModel(0.4, 3.5, 4.0, 2, 3));
		data.add(new ClassificationFreebetUnlockerModel(0.6, 4.0, 5.0,3, 4));
		data.add(new ClassificationFreebetUnlockerModel(1.0, 5.0, 1000000.0, 4, 10000));

		ObjectDataCompiler converter = new ObjectDataCompiler();
		String drl = converter.compile(data, template);
		System.out.println(drl);

		KieSession ksession = createKieSessionFromDRL(drl);

		List<Pair> pairs = new ArrayList<>();
		pairs.add(new Pair(1L, "HOME", "AWAY", "X", 3.05));

		Ticket ticket = new Ticket(1L, pairs, 300.0);
		Client client = new Client(1L, "mejl", "Imenko", "Prezimenkovic", 1000.0, 15000.0, 0.0, 1, new ArrayList<>(), "");
		ksession.insert(ticket);
		ksession.insert(client);
		ksession.fireAllRules();

		assertEquals(client.getFreebet(), 60.0);

		Ticket ticket1 = new Ticket(2L, pairs, 300.0);
		Client client1 = new Client(2L, "mejl", "Imenko", "Prezimenkovic", 1000.0, 15000.0, 0.0, 1, new ArrayList<>(), "");
		ticket1.addPair(new Pair(1L, "HOME", "AWAY", "X", 3.05));
		System.out.println(ticket1.getFullOdd());
		ksession.insert(ticket1);
		ksession.insert(client1);
		ksession.fireAllRules();
		assertEquals(client1.getFreebet(), 300.0);

	}
	private KieSession createKieSessionFromDRL(String drl)
	{
		KieHelper kieHelper = new KieHelper();
		kieHelper.addContent(drl, ResourceType.DRL);

		Results results = kieHelper.verify();

		if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
			List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
			for (Message message : messages) {
				System.out.println("Error: "+message.getText());
			}

			throw new IllegalStateException("Compilation errors were found. Check the logs.");
		}

		return kieHelper.build().newKieSession();
	}


}
